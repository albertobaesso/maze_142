﻿(*
* LabProg2019 - Progetto di Programmazione a.a. 2019-20
* Maze.fs: maze
* (C) 2019 Alvise Spano' @ Universita' Ca' Foscari di Venezia
*)
(* References:
   - maze generation:
   https://en.wikipedia.org/wiki/Maze_generation_algorithm#Depth-first_search 
   https://rosettacode.org/wiki/Maze_generation#F.23 
   - maze solution :
   https://www.cs.bu.edu/teaching/alg/maze/

   Work by Baesso Alberto , Basile Giorgio , Danci Romina Alina
*)
module LabProg2019.Maze
open System
open External
open Gfx


let rnd = Random()

type CharInfo with
    /// Shortcut for creating a wall pixel.
    static member wall = pixel.create (Config.wall_pixel_char, Color.White)
    /// Shortcut for creating a path pixel.
    static member internal path = pixel.filled Color.DarkCyan
    /// Shortcut for creating a route pixel.
    static member route = pixel.create(Config.route_pixel_char, Color.Magenta,Color.DarkCyan) //vaporwave done E|_(^-^)
    /// Check whether this pixel is a wall.
    member this.isWall = this = pixel.wall

// TODO: implement the maze type, its generation (task 1) and its automatic resolution (task 2) 

type Maze (w:int , h:int ) as this  = //w,h must be odd numbers

    let mazeGrid = Array2D.init<CharInfo> h w (fun x y -> if x%2<> 0 || y%2<> 0 then CharInfo.wall else CharInfo.path) 
    let checkGrid = Array2D.zeroCreate<bool> ((Array2D.length1 mazeGrid / 2) + 1) ((Array2D.length2 mazeGrid / 2) + 1) 
    let mutable routeList : (int*int) list = []
    // TODO: do not forget to call the generation function in your object initializer
    do this.Generate  
            
    /// Convert a given 2D array to a 1D array with W*H  cells
    member private __.ConvertToArray (mazeGrid) = 
        seq {
            for i in 0 .. (Array2D.length1 mazeGrid - 1) do
                for j in 0 ..( Array2D.length2 mazeGrid - 1) ->
                    mazeGrid.[i,j]
        }
            |> Seq.toArray

    /// CharInfo Array with the generated maze
    member public __.CompleteMaze = __.ConvertToArray mazeGrid

    /// List of positions to get from start location to end location
    member public __.RealRoute = routeList

    /// Maze height
    member public __.Height = h

    /// Maze width
    member public __.Width = w

    ///Check if the selected location has beel visited if pattern is 0. 
    ///If the pattern is 1 check also if that position is wall 
    member private __.visitedLocation (x,y) (grid:bool[,]) pattern =
        match pattern with
        0 -> not grid.[x,y]
        |1 -> not grid.[x,y] && (mazeGrid.[x,y]<> CharInfo.wall)
        |_  -> true

    /// List of accessible future position given a start position and a grid 
    member private __.CheckNeighbors (x,y) grid tipo =
        let adjacent(x1,y1) =
           x1 >= 0 && x1 < (Array2D.length1 grid) && y1 >= 0 && y1 < (Array2D.length2 grid)

        let possibleLocation = Array.filter adjacent [|(x,y+1);(x,y-1);(x+1,y);(x-1,y)|]

        Array.filter (fun z -> __.visitedLocation (z) grid  tipo ) possibleLocation
 
    /// Take in input a start position an end position and a 2D array and change the value of the cell between the two position
    member private __.DestroyWall (x,y)(x1,y1) (m:CharInfo[,])=
        match (x,y) with
         (x,y) when x>x1 -> m.[((x*2) - 1), (y*2)] <- CharInfo.path
        |(x,y) when x<x1 -> m.[((x*2) + 1), (y*2)] <- CharInfo.path
        |(x,y) when y>y1 -> m.[(x*2), ((y*2) - 1)] <- CharInfo.path
        |_ ->  m.[(x*2), ((y*2) + 1)] <- CharInfo.path

    ///Generate the maze path given a starting point and a location 2D Array        
    member private __.GenPath (x,y) (checkGrid:bool[,])=

        checkGrid.[x,y] <- true  // now this location has been visited
        let mutable neighbors = __.CheckNeighbors (x,y) checkGrid 0
            
        if not (Array.isEmpty neighbors) then
            let nextLocation = neighbors.[rnd.Next(Array.length neighbors )]
            __.DestroyWall (x,y) (nextLocation) mazeGrid
            __.GenPath (nextLocation) checkGrid
        else 
            for i in 0.. (Array2D.length1 checkGrid - 1 ) do
                for j in 0.. (Array2D.length2 checkGrid - 1) do
                    if checkGrid.[i,j] && not (Array.isEmpty (__.CheckNeighbors (i,j) checkGrid 0 ))
                    then __.GenPath (i,j) checkGrid 

    ///Start maze generation
    member private __.Generate = 
        __.GenPath (0,rnd.Next(0,Array2D.length2 checkGrid)) checkGrid

    /// Remove positions from a list until it find a position with accessible neighbors location 
    member private __.RewindList list grid =
        match list with
        []->list
        |[x]->list
        |x::(y,z)::xs ->
                        match (y,z) with
                        (y,z) when not (Array.isEmpty (__.CheckNeighbors (y,z) grid 1)) -> (y,z)::xs
                        |_ ->  __.RewindList ((y,z)::xs) grid

  
    /// Fills routeList with the locations that take you from start position to end position
    member public __.AutoSolve  (check:bool[,]) (x0,y0) (x1,y1)  = 
        
        routeList <- (x0,y0)::routeList
        check.[x0,y0] <- true

        if y0 < (w - 1) && mazeGrid.[x0,y0+1] = CharInfo.path && not (check.[x0,y0+1]) && (x0  ,(y0 + 1)) <> (x1,y1) then 
            __.AutoSolve check (x0,(y0 + 1)) (x1,y1)  // move right
        else if x0 < (h - 1) && mazeGrid.[x0 + 1,y0] = CharInfo.path && not (check.[x0+1,y0]) && (x0 + 1 ,y0) <> (x1,y1) then 
            __.AutoSolve check ((x0 + 1),y0) (x1,y1)  // move down
        else if y0> 0 && mazeGrid.[x0,y0 - 1] = CharInfo.path && not (check.[x0,y0-1]) && (x0 ,y0 - 1) <> (x1,y1) then
            __.AutoSolve check (x0 ,(y0 - 1)) (x1,y1)  //move left
        else if x0 > 0 && mazeGrid.[x0-1,y0] = CharInfo.path && not (check.[x0-1,y0]) && (x0 - 1 ,y0) <> (x1,y1) then 
            __.AutoSolve check ((x0 - 1 ),(y0)) (x1,y1) //move up
        else if (Array.isEmpty (__.CheckNeighbors (x0,y0) check 1)) && (x0  ,(y0 + 1)) <> (x1,y1) && (x0 + 1 ,y0) <> (x1,y1) && (x0 ,y0 - 1) <> (x1,y1) && (x0 - 1 ,y0) <> (x1,y1) then
            routeList <- __.RewindList routeList check
            __.AutoSolve check routeList.Head (x1,y1)
        
    ///Generate a number , must be an odd number or it could be a wall position
    member public __.CanBePoint first last = 
        let point = rnd.Next(first,(last + 1))
        match point with
        point when point % 2 <> 0 -> point 
        |_ -> __.CanBePoint first last
    
    //Clear the routeList list so it can be used again
    member public __.ClearRoute ls =
        routeList <- []


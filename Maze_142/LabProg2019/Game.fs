﻿module LabProg2019.Game

open Engine
open Gfx
open LabProg2019.Maze
open System
open External

[< NoEquality; NoComparison >]
type state = {
    player : sprite 
}

let W = 89  //smallest possible is 15 , must be an odd number
let H = 39  //smallest possible is 3 , must be an odd number --> under 56 if you want to stay in the screen

let mainInteractive () =     

    //-------------------- Variables part --------------------

    let engine = new engine (W, H)
    let maze_w = W - 2 // -2 <- must be an odd number , must be at least screen - 2
    let maze_h = H - 2
    let m = Maze(maze_w, maze_h)
    let startY = 1
    let startX = m.CanBePoint 1 (maze_w / 2) 
    let endX = m.CanBePoint (maze_w / 2) maze_w  
    let endY = maze_h

    let player = let d = 1 in engine.create_and_register_sprite (W - 3, H - 3,startX, startY, 300)
    let routeSprite =  engine.create_and_register_sprite (W - 1 , H - 1, 1, 1, 150)
    let st0 = { 
        player = player                         
        }

    
    //-------------------- Graphics part --------------------


    let rec drawRoute ls =
        match ls with
        []-> routeSprite.draw_text (sprintf "Movements: WASD and arrows Solution: Spacebar  Quit: Q", 0, H - 2, Color.Black, Color.White)
        |(x,y)::xs-> routeSprite.draw_rectangle(y,x,1,1,CharInfo.route)
                     drawRoute xs        
    
    let rec clearRoute ls =
        match ls with
        []-> routeSprite.draw_text (sprintf "Movements: WASD and arrows Solution: Spacebar  Quit: Q", 0, H - 2, Color.Black, Color.White)
        |(x,y)::xs-> routeSprite.draw_rectangle(y,x,1,1,CharInfo.path)
                     drawRoute xs 
    ignore <| engine.create_and_register_sprite (image.rectangle (W, H, CharInfo.wall), 0, 0, -1)
    ignore <| engine.create_and_register_sprite (image (maze_w,maze_h ,m.CompleteMaze),1,1,100)
    ignore <| engine.create_and_register_sprite (image.rectangle (1,1, pixel.filled Color.Red),startX,startY,200)
    ignore <| engine.create_and_register_sprite (image.rectangle (1,1, pixel.filled Color.Green),endX,endY,200)
    
    player.draw_text ("\190", 0, 0, Color.Red,Color.DarkCyan)
    routeSprite.draw_text (sprintf "Movements: WASD and arrows Solution: Spacebar  Quit: Q", 0, H - 2, Color.Black, Color.White)

    let print_end () =
        let win = engine.create_and_register_sprite (W - 2 , H - 2, 1, 1, 400)
        win.flood_fill(1,1,CharInfo.path)
        let grafiti = engine.create_and_register_sprite (W - 2 , H - 2, 1, 1, 500)
        grafiti.draw_text("\219     \219  \220\223\223\220  \219   \219      \219  \219  \219  \220\223\223\220  \219\220   \219",win.width/4,(win.height/4)+6,Color.White,Color.DarkCyan)
        grafiti.draw_text(" \223\220 \220\223  \219    \219 \219   \219      \219  \219  \219 \219    \219 \219 \223\220 \219",win.width/4,(win.height/4)+7,Color.White,Color.DarkCyan)
        grafiti.draw_text("   \219     \223\220\220\223  \223\220\220\220\223      \223\220\220\223\220\220\223  \223\220\220\223  \219   \223\219",win.width/4,(win.height/4)+8,Color.White,Color.DarkCyan)
    
    
    //-------------------- Update part --------------------
    
    let my_update (key : ConsoleKeyInfo) (screen : wronly_raster) (st : state) =
        // move player\\
        let dx, dy =
            match key.Key with 
            | ConsoleKey.W | ConsoleKey.UpArrow-> 0., -1.
            | ConsoleKey.S | ConsoleKey.DownArrow-> 0., 1.
            | ConsoleKey.A | ConsoleKey.LeftArrow-> -1., 0.
            | ConsoleKey.D | ConsoleKey.RightArrow -> 1., 0.
            | _   -> 0., 0.
   
        match key.Key with
            | ConsoleKey.Spacebar ->    routeSprite.clear
                                        m.ClearRoute (m.RealRoute)
                                        m.AutoSolve (Array2D.zeroCreate<bool> m.Height m.Width)(int st.player.y - 1,int st.player.x - 1)(endY-1,endX-1)  
                                        drawRoute (m.RealRoute)
       
            | _ -> ()
        // TODO: check bounds -> screen -> reduced to path min/max , walls ->  done 
        if (int (st.player.x + dx) > 0 && int (st.player.y + dy) > 0 && int (st.player.x + dx) <= maze_w && int (st.player.y + dy) <= maze_h ) then
            if not (m.CompleteMaze.[  int (st.player.x - 1. + dx) + maze_w * int (st.player.y - 1. + dy) ].isWall) then
                st.player.move_by (dx, dy)
                if int st.player.x = endX && int st.player.y = endY then 
                    print_end()
        st, key.Key = ConsoleKey.Q

                               
    engine.loop_on_key my_update st0         


let mainHelp () =

    //-------------------- Variables part --------------------

    let engine = new engine (W, H)
    ignore <| engine.create_and_register_sprite (image.rectangle (W, H, pixel.filled Color.White, pixel.filled Color.DarkCyan), 0, 0, 50)
    let player = engine.create_and_register_sprite (image.rectangle (W, H, pixel.filled Color.White, pixel.filled Color.DarkCyan), 0, 0, 100)
    let st0 = { 
        player = player
        }
    
    //-------------------- Graphics part --------------------
    
    player.draw_text(" __  __                         _\n|  \/  | __ _ _ __  _   _  __ _| |\n| |\/| |/ _` | '_ \| | | |/ _` | |\n| |  | | (_| | | | | |_| | (_| | |\n|_|  |_|\__,_|_| |_|\__,_|\__,_|_|",((W/4)+2),H/18,Color.White,Color.DarkCyan)
    player.draw_text("Goal of the game\n   Reach the end of the maze by moving the player with the use of the buttons:\n    W or UpArrow -->to move up\n    S or DownArrow -->to move down\n    A or LeftArrow -->to move left\n    D or RightArrow -->to move right \n\n  For the solution of the maze use the button --> Spacebar",W/30,H/4,Color.White,Color.DarkCyan)

    //-------------------- Update part --------------------

    let game (key : ConsoleKeyInfo) (screen : wronly_raster) (st : state) =
        st, key.Key = ConsoleKey.Q

    engine.loop_on_key game st0


/// Main Menu . Need to be called as first item
let mainMenu () = 

        //-------------------- Variables part --------------------
        
        let engine = new engine (W, H)
        let player = engine.create_and_register_sprite (image.rectangle (W, H, pixel.filled Color.White, pixel.filled Color.DarkCyan), 0, 0, 100)
        let st0 = { 
            player = player
            }

        //-------------------- Graphics part --------------------

        ignore <| engine.create_and_register_sprite (image.rectangle (W, H, pixel.filled Color.White, pixel.filled Color.DarkCyan), 0, 0, 50)
        player.draw_text(" ____    ____        _        ________   ________      __     _    _      _____\n|_   \  /   _|      / \      |  __   _| |_   __  |    /  |   | |  | |    / ___ `. \n  |   \/   |       / _ \     |_/  / /     | |_ \_|    `| |   | |__| |_  |_/___) | \n  | |\  /| |      / ___ \       .'.' _    |  _| _      | |   |____   _|  .'____.' \n _| |_\/_| |_   _/ /   \ \_   _/ /__/ |  _| |__/ |    _| |_      _| |_  / /_____  \n|_____||_____| |____| |____| |________| |________|   |_____|    |_____| |_______| ",W/40,H/18,Color.Magenta,Color.DarkCyan)
        player.draw_text(" _____  _\n|  __ \| |\n| |__) | | __ _ _   _\n|  ___/| |/ _` | | | |\n| |    | | (_| | |_| |\n|_|    |_|\__,_|\__, |\n                 __/ |\n                |___/\n",((W/4)+10),((H/4)+2),Color.White,Color.DarkCyan)
        player.draw_text(" _    _      _\n| |  | |    | |\n| |__| | ___| |_ __\n|  __  |/ _ \ | '_ \ \n| |  | |  __/ | |_) |\n|_|  |_|\___|_| .__/ \n              | |\n              |_|",((W/4)+10),((H/2)+2),Color.White,Color.DarkCyan)
        player.draw_text("To start the game press 'p'\nFor instraction press 'h'\nMaze 142 Baesso Alberto,Basilie Giorgio e Danci Romina ",((W/20)-1),((H/2)+15),Color.White,Color.DarkCyan)
    
       
        //-------------------- Update part --------------------
        
        let game (key : ConsoleKeyInfo) (screen : wronly_raster) (st : state) =
            match key.Key with 
            |ConsoleKey.P-> mainInteractive ()
            |ConsoleKey.H-> mainHelp ()
            |_ -> ()
            st, key.Key = ConsoleKey.Q

        engine.loop_on_key game st0